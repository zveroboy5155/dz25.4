#pragma once
#include <string>

struct pos {
	double x;
	double y;
	pos(double val);
	pos();
	
	pos operator = (pos &new_pos) {
		this->x = new_pos.x;
		this->y = new_pos.y;
		return *this;
	}
	
	bool operator == (pos &sec_pos) {
		return (this->x == sec_pos.x && this->y == sec_pos.y);
	}
	void in_pos();
	std::string pos_str();
};

/*std::istream& operator >> (std::istream& in, pos& new_pos)
{
	return in >> new_pos.x >> new_pos.y;
};*/

/*std::basic_istream& operator >> (std::basic_istream& in, pos& new_pos)
{
	return  in >> new_pos.x >> new_pos.y;
}*/


/*std::ostream& operator << (std::ostream& out, const pos& cur_pos)
{
	return out << cur_pos.x << cur_pos.y;
};*/

struct line {
	pos start;
	pos end;
	line(double val);
	line();
	std::string line_str();
};

