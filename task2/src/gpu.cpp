#include "gpu.h"
#include <iostream>
#include <vector>
#include "ram.h"


bool print_ram()
{	
	std::vector<int> *ram_vec = get_ram_buff();

	for (auto ptr : *ram_vec ) {
		std::cout << ptr << " ";
	}
	std::cout << std::endl;
	return false;
}
