#include "disk.h"
#include "ram.h"
#include <iostream>
#include <vector>
#include <fstream>

char file_path[] = "./data.txt";

bool save() {
	std::ofstream file;
	std::vector <int> *buff;

	file.open(file_path);
	buff = get_ram_buff();
	for (auto p : *buff) {
		file << p << " ";
	}

	file.close();
	return true;
}

bool load()
{
	std::ifstream file;
	std::vector <int> buff;
	int cur_num;

	file.open(file_path);
	for (int i = 0; i < 8; i++) {
		file >> cur_num;
		buff.push_back(cur_num);
	}
	put_ram_buff(buff);
	file.close();
	return true;
}
