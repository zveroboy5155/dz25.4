#include "include/task2_main.h"
#include "cpu.h"
#include "kdb.h"
#include "gpu.h"
#include "disk.h"
#include <iostream>
#include <string>

/*
  sum		- ��������� �����
  save		- ��������� � ����
  load		- ��������� �� �����
  input		- ������ � ����������
  display	- ������� �� �����
  exit		- �����
*/


void task2() {
	std::string cmd;
	std::cout << "������ �� task �2" << std::endl;
	while (true) {
		std::cout << "������� ������� (sum, save, load, input, display, exit)" << std::endl;
		std::cin >> cmd;
		if (cmd == "exit") break;
		else if (cmd == "sum") computer();
		else if (cmd == "input") enter_num();
		else if (cmd == "display") print_ram();
		else if (cmd == "save") save();
		else if (cmd == "load") load();
		else std::cout << "������� �� �����������" << std::endl;
	}
	//computer();
}